# news_for_me

A simple Flutter application that takes a list of news using the public API
from newsapi.org and outputs a list of cards with article photos, headings and descriptions.

## Used technologies and packages

- async init application with FutureBuilder
- BloC with StreamBuilder for update widgets states
- lazy card-list with ListView.builder
- http -- library for making HTTP requests
- public API from newsapi.org

## Getting Started

This project is an example of working with `Bloc` state management
technology and working with external APIs.

A few resources to help you get started with Flutter:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
