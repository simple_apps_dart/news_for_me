// newsapi.org

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:news_for_me/configs/app_config.dart';
import 'package:news_for_me/models/newsInfo.dart';

// ignore: camel_case_types
class NewsApi {
  Future<NewsInfo> getNews() async {
    await AppConfig().initialize();
    var newsInfo = {
      'status': '',
      'errorMessage': '',
      'totalResults': 0,
      'articles': []
    };
    var client = http.Client();

    try {
      // var headers = {
      //   'Content-Type': 'application/json; charset=UTF-8',
      // };
      // var response = await client.get(AppConfig().apiUrl, headers: headers);
      var response = await client.get(AppConfig().apiUrl);
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);
      if (response.statusCode == 200) {
        var newsInfo = NewsInfo.fromJson(jsonMap);
        return newsInfo; // NewsInfo.fromJson(jsonMap);
      }
      newsInfo['status'] = 'error';
      newsInfo['errorMessage'] =
          'http.response.statusCode == ${response.statusCode}';
      return NewsInfo.fromJson(newsInfo);
    } on http.ClientException catch (e) {
      newsInfo['status'] = 'error';
      newsInfo['errorMessage'] = e.message;
    } on FormatException catch (e) {
      newsInfo['status'] = 'error';
      newsInfo['errorMessage'] =
          '${e.message}\nsource: ${e.source}\noffset: ${e.offset}';
    } on Exception catch (e) {
      newsInfo['status'] = 'error';
      newsInfo['errorMessage'] = e.toString();
    }
    return NewsInfo.fromJson(newsInfo);
  }
}
